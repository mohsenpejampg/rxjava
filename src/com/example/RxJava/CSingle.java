package com.example.RxJava;

import io.reactivex.Single;

/**
 * Single
 * Emits either a single item or an error event. the reactive version of a method call
 */

class CSingle {

  void onStart() {
    System.out.println("Single onStart");
  }
}
