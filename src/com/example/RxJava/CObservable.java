package com.example.RxJava;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;

/**
 * Observable
 * Emits 0 or n items and terminates with a success or an error events
 * Observable is main concept of reactivex
 * Observable is an interface and main method is subscribe
 * its lazy => wait for subscribe
 * <p>
 * Observable is fully like Iterator
 * <p>
 * Observer
 * is an interface, after subscribe, it will be call from functions
 * <p>
 * <p>
 * Operator on Observable => work on Observable
 * return Observable
 * can be chained
 */
class CObservable {
  void onStart() {
    System.out.println("Observable onStart");
    Observable observable = Observable.fromArray(1, 2, 3, 4, 5);
    funcMap(observable);
    //funcFilter(observable);
  }

  private void funcFilter(Observable observable) {
    Observer observer = new Observer() {
      @Override
      public void onSubscribe(Disposable disposable) {

      }

      @Override
      public void onNext(Object o) {
        System.out.println(o);
      }

      @Override
      public void onError(Throwable throwable) {

      }

      @Override
      public void onComplete() {
        System.out.println("onComplete funcFilter");
      }
    };

    observable.filter(o -> (int) o % 2 == 0).subscribe(observer);
  }

  private void funcMap(Observable observable) {
    Observer observer = new Observer() {
      @Override
      public void onSubscribe(Disposable disposable) {

      }

      @Override
      public void onNext(Object o) {
        System.out.println(o);
      }

      @Override
      public void onError(Throwable throwable) {

      }

      @Override
      public void onComplete() {
        System.out.println("onComplete funcMap");
      }
    };
    //observable.map(o -> (int) o * 2).subscribe(observer);
    observable.map(o -> (int) o * 2).subscribe(o -> System.out.println(o) ,throwable -> System.out.println(throwable.getClass()));
    observable.map(o -> (int) o * 2).subscribe(o -> System.out.println(o));

    observable.map(new Function() {
      @Override
      public Object apply(Object o) throws Exception {
        System.out.println((int) o * 3);
        return null;
      }
    });
  }

}
