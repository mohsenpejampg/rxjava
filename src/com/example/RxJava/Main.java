package com.example.RxJava;

public class Main {

  public static void main(String[] args) {

    new CObservable().onStart();
    //new CSingle().onStart();
    //new CIterator().onStart();

    /**
     * Type of Programming
     */
    /**
     *  Declarative -ˈklar-,diˈkle(ə)rətiv (اعلانی)
     *  what the program must accomplish rather than describe how to accomplish it as a sequence of programming language primitive
     *
     *  Imperative (امری)
     *  imperative programming is a programming paradigm that uses statements that change a program's state.
     *  exm : open the door
     *
     *  Diagram =>
     *            Declarative programming
     *            Imperative programming Language(java)
     *            Assembly
     *            Binary Code
     *
     *
     * reactive
     * reactive programming is a declarative programming paradigm concerned with data streams and the propagation of change
     *
     */

    /**
     * Type Schedulers
     *    - each operator has default Schedulers
     *    - use on change thread for use on observer or subscriber
     *      observeOn()
     *      subscribeOn()
     */
    /**
     * Computation
     *    handle thread or looper
     *
     * IO
     *
     * Main Thread
     *    ui thread in android
     *
     * New Thread
     *    create several thread
     *
     * Executor
     *
     *
     * Looper
     */



  }
}
